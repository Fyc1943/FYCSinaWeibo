//
//  WBWebViewController.swift
//  SinaWeiBo
//
//  Created by Frank on 2017/9/29.
//  Copyright © 2017年 Frank. All rights reserved.
//

import UIKit

class WBWebViewController: WBBaseViewController {

   lazy var webView = UIWebView(frame: UIScreen.main.bounds)
   
   /// 懒加载的URL 字符串
    var urlString: String? {
        didSet {
            guard let urlString = urlString,
            let url = URL(string: urlString)
            else {
                return
            }
            webView.loadRequest(URLRequest(url:url))
        }
    }
}

extension WBWebViewController {
    override func setUpTableView() {
        // 设置标题
        navItem.title = "网页"
        // 设置webView
        view.insertSubview(webView, belowSubview: navigationBar)
        
        webView.backgroundColor = UIColor.white
        // 设置contentInset
        webView.scrollView.contentInset.top = navigationBar.bounds.height
    }
}


