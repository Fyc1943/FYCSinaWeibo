//
//  WBComposeViewController.swift
//  SinaWeiBo
//
//  Created by Frank on 2017/9/27.
//  Copyright © 2017年 Frank. All rights reserved.
//

import UIKit
// 攥写微博控制器
class WBComposeViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var toolbar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func close()  {
        dismiss(animated: true, completion: nil)
    }
    lazy var sendButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("发布", for: [])
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        // 设置标题颜色
        btn.setTitleColor(UIColor.white, for: [])
        btn.setTitleColor(UIColor.gray, for: .disabled)
        // 设置背景图片
        btn.setBackgroundImage(UIImage(named: "common_button_orange"), for: [])
        btn.setBackgroundImage(UIImage(named: "common_button_orange_highlighted"), for: .highlighted)
        btn.setBackgroundImage(UIImage(named: "common_button_orange_highlighted"), for: .disabled)

        // 设置大小
        btn.frame = CGRect(x: 0, y: 0, width: 45, height: 35)
        return btn
    }()
}
extension WBComposeViewController {
    
    func setupUI() {
        view.backgroundColor = UIColor.white
        setupNavigationBar()
    }
    func setupNavigationBar()  {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "关闭", target: self, action: #selector(close))
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: sendButton)
        sendButton.isEnabled = false
    }
}

