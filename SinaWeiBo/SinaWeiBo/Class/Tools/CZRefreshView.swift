//
//  CZRefreshView.swift
//  SinaWeiBo
//
//  Created by Frank on 2017/9/23.
//  Copyright © 2017年 Frank. All rights reserved.
//

import UIKit

class CZRefreshView: UIView {
    /// 刷新状态
    /**
     ios 系统中 UIView 封装的旋转动画
     -  默认顺时针旋转
     -  就近原则
     -  要想实现方向旋转，需要调整一个非常小的数字
     -  如果想实现360旋转,需要核心动画， CABaseAnimation
     */
    var refreshState: CZRefreshState = .Normal{
        didSet {
            switch refreshState {
            case .Normal:
                // 恢复状态
                tipIcon?.isHidden = false
                indicator?.stopAnimating()
                
                tipLabel?.text = "继续使劲拉..."
                UIView.animate(withDuration: 0.25, animations: {
                    self.tipIcon?.transform = CGAffineTransform.identity
                });
            case .Pulling:
                tipLabel?.text = "放手就刷新..."
                UIView.animate(withDuration: 0.25, animations: {
                    self.tipIcon?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI-0.001))
                });
            case .WillRefresh:
                tipLabel?.text = "正在刷新中..."
                // 隐藏提示图标
                tipIcon?.isHidden = true
                // 显示菊花
                indicator?.startAnimating()
            }
        }
    }
    /// 父视图高度 - 为了刷新控件不需要关心当前具体的刷新视图是谁
    var parentViewHeight: CGFloat = 0
    
    // 提示图标
    @IBOutlet weak var tipIcon: UIImageView?
    // 提示标签
    @IBOutlet weak var tipLabel: UILabel?
    // 指示器
    @IBOutlet weak var indicator: UIActivityIndicatorView?
   
    class func refreshView() -> CZRefreshView {
        let nib = UINib(nibName: "CZMeituanRefreshView", bundle:nil)
        return nib.instantiate(withOwner:nil, options: nil)[0] as! CZRefreshView
    }
}
