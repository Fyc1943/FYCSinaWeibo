//
//  CZEmoticonAttachment.swift
//  SinaWeiBo 表情键盘
//
//  Created by Frank on 2017/9/28.
//  Copyright © 2017年 Frank. All rights reserved.
//

import UIKit

// 表情附件
class CZEmoticonAttachment: NSTextAttachment {
    /// 表情纯文本，用于发送给服务器
    var chs: String?
}
